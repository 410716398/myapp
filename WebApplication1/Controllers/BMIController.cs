﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.ViewModel;

namespace WebApplication1.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index()
        {
            return View(new BMIData());
        }
        // GET: BMI
        [HttpPost]
        public ActionResult Index(BMIData data)
        {

            if (ModelState.IsValid)
                
            {
                var m_height = data.Height / 100;
                var resule = data.Weight / (m_height * m_height);
                var level = "";

                if (resule < 18.5)
                {
                    level = "體重過輕";
                }
                else if (18.5 <= resule && resule <= 24)
                {
                    level = "正常範圍";
                }
                else if (24 <= resule && resule <= 27)
                {
                    level = "過重";
                }
                else if (27 <= resule && resule <= 30)
                {
                    level = "輕度肥胖";
                }
                else if (30 <= resule && resule <= 35)
                {
                    level = "中度肥胖";
                }
                else if (35 <= resule)
                {
                    level = "重度肥胖";
                }


                data.Result = resule;
                data.Level = level;
            }

            return View(data);
        }

        
    }
}
